﻿using OpenQA.Selenium;
using StoreAutomation.Page_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreAutomation
{
    public static class StoreSetup
    {
        public static Cart CartPage;
        public static ProductDetail ProductDetailPage;
        public static Search SearchPage;
        public static BaseComponent BaseComponentPage;

        public static void PageInitializer(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");

            BaseComponentPage = new BaseComponent(driver);
            CartPage = new Cart(driver);
            ProductDetailPage = new ProductDetail(driver);
            SearchPage = new Search(driver);
        }
    }
}
