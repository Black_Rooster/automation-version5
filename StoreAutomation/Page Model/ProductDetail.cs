﻿using OpenQA.Selenium;

namespace StoreAutomation.Page_Model
{
    public class ProductDetail : BaseComponent
    {
        public ProductDetail(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement SendToFriendLink => Driver.FindElement(By.Id("send_friend_button"));

        public IWebElement AddToCartButton => Driver.FindElement(By.Name("Submit"));

        public IWebElement FriendNameInput => Driver.FindElement(By.Id("friend_name"));

        public IWebElement FriendEmailInput => Driver.FindElement(By.Id("friend_email"));

        public IWebElement SendEmailButton => Driver.FindElement(By.Id("sendEmail"));

        public IWebElement ErrorMessage => Driver.FindElement(By.Id("send_friend_form_error"));

        public IWebElement SuccessModal => Driver.FindElement(By.XPath("//div[@class='fancybox-wrap fancybox-desktop fancybox-type-html fancybox-opened']"));
    }
}
