﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreAutomation.Page_Model
{
    public class Cart : BaseComponent
    {
        public Cart(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement ProductCounter => Driver.FindElement(By.ClassName("ajax_cart_no_product"));

        public IWebElement RemoveProduct => Driver.FindElement(By.XPath("//a[@title='Delete']"));

        public IWebElement CartProductLink => Driver.FindElement(By.LinkText("Faded Short Sleeve T-shirts"));

        public IWebElement ProductTotal => Driver.FindElement(By.Id("total_product"));

        public IList<IWebElement> ProductPriceList => Driver.FindElements(By.XPath("//td[@data-title='Total']//span"));

        public void WaitForProductCounterUpdate()
        {
            DriverWait.Until(value => ProductCounter.Displayed == true);
        }
    }
}
