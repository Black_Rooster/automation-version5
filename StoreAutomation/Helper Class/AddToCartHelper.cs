﻿namespace StoreAutomation.Helper_Class
{
    public static class AddToCartHelper
    {
        public static void ProccedToCart()
        {
            StoreSetup.SearchPage.ProductLink.Click();
            StoreSetup.ProductDetailPage.AddToCartButton.Click();
            StoreSetup.BaseComponentPage.ProceedToCheckoutButton.Click();
        }
    }
}
