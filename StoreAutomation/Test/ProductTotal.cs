﻿using NUnit.Framework;
using StoreAutomation.Helper_Class;

namespace StoreAutomation.Test
{
    [TestFixture]
    public class ProductTotal : Base
    {
        [Test]
        public void GivenTwoProductInACart_WhenConfirmingTotal_ThenAddTheirPriceAndCompareItWithTotalPrice()
        {
            StoreSetup.BaseComponentPage.HoverEffect(StoreSetup.BaseComponentPage.ProductList[0]);
            StoreSetup.BaseComponentPage.AddToCart(1).Click();
            StoreSetup.BaseComponentPage.Close.Click();

            StoreSetup.BaseComponentPage.HoverEffect(StoreSetup.BaseComponentPage.ProductList[1]);
            StoreSetup.BaseComponentPage.AddToCart(2).Click();
            StoreSetup.BaseComponentPage.Close.Click();

            StoreSetup.BaseComponentPage.ViewCart.Click();

            var productTotal = ProductTotalHelper.GetProductPrice(StoreSetup.CartPage.ProductList);
            
            Assert.AreEqual(StoreSetup.CartPage.ProductTotal.Text, productTotal);
        }
    }
}
