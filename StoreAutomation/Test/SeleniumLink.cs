﻿using NUnit.Framework;

namespace StoreAutomation.Test
{
    [TestFixture]
    public class SeleniumLink : Base
    {
        [Test]
        public void GivenALink_WhenTheLinkIsClicked_ThenUserIsDirectToANewUrl()
        {
            var expected = "http://www.seleniumframework.com/";

            StoreSetup.BaseComponentPage.SeleniumFrameworkLink.Click();

            Assert.AreEqual(expected, StoreSetup.BaseComponentPage.CurrentURL);
        }
    }
}
