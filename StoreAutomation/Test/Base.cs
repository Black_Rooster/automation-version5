﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace StoreAutomation.Test
{
    public class Base
    {
        private IWebDriver _driver;

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

            StoreSetup.PageInitializer(_driver);
        }

        [TearDown]
        public void Shutdown()
        {
            _driver.Close();
        }
    }
}
