﻿using NUnit.Framework;
using StoreAutomation.Helper_Class;

namespace StoreAutomation.Test
{
    [TestFixture]
    public class AddToCart : Base
    {
        private readonly string _productName = "Faded Short Sleeve T-shirts";

        [SetUp]
        public void SearchProduct()
        {
            StoreSetup.BaseComponentPage.SearchField.SendKeys(_productName);
            StoreSetup.BaseComponentPage.SearchButton.Click();
        }

        [Test]
        public void GivenProductName_WhenAdddingToCartWithinProductDetails_ThenAddToCart()
        {
            AddToCartHelper.ProccedToCart();

            Assert.IsTrue(StoreSetup.CartPage.CartProductLink.Displayed, "Product was not added");
        }

        [Test]
        public void GivenProductName_WhenAdddingToCartFromSearchResult_ThenAddToCart()
        {
            StoreSetup.BaseComponentPage.HoverEffect(StoreSetup.SearchPage.ProductLink);
            StoreSetup.BaseComponentPage.AddToCart(1);
            StoreSetup.ProductDetailPage.ProceedToCheckoutButton.Click();

            Assert.IsTrue(StoreSetup.CartPage.CartProductLink.Displayed, "Product was not added");
        }

        [Test]
        public void GivenProductName_WhenRemovingProductInTheCart_ThenRemoveProduct()
        {
            AddToCartHelper.ProccedToCart();
            StoreSetup.CartPage.RemoveProduct.Click();
            StoreSetup.CartPage.WaitForProductCounterUpdate();

            Assert.AreEqual("(empty)", StoreSetup.CartPage.ProductCounter.GetAttribute("textContent"));
        }
    }
}
